// TODO: 1. run rg "tags::.*recipe" to get a list of file names
const rg = Deno.run({
  cmd: ["rg", "-l", "-tmd", "tags::.*recipe", "."],
  stderr: "piped",
  stdout: "piped",
});
const [status, stdout, stderr] = await Promise.all([
  rg.status(),
  rg.output(),
  rg.stderrOutput(),
]);
const recipeFiles = new TextDecoder().decode(stdout).trim().split("\n");
rg.close();

// TODO: 2. load files
// TODO: 4 - Promise all and combine
let json = {};
for (let file of recipeFiles) {
  console.log("1");
  const text = await Deno.readTextFile(file);
  console.log("2");

  json = parse(text.split("\n"), json, { count: 0 }, true);
}

console.log("json", JSON.stringify(await json));

// TODO: 3. load as markdown tree
export {};

interface Current {
  h1?: string;
  h2?: string;
  h3?: string;
  h4?: string;
  h5?: string;
  h6?: string;
  paragraph?: string;
  inParagraph?: boolean;
  list?: string;
  inList?: boolean;
  count: number;
  hash?: string;
}

async function parse(text, json, current: Current, initialSetup) {
  // create hash and store in it
  if (initialSetup) {
    let digest = await digestMessage(text);
    digestMessage(text).then((digest) => {});
    current.hash = digest;
    initialSetup = false;
    console.log("BEFORE", json);
    json = { ...json, [digest]: {} };
    console.log("AFTER", json);
  }

  if (text.length === 0) {
    return json;
  }

  // handle tags
  if (!(json[current.hash]?.frontmatter?.tags?.length > 0)) {
    let node = text.shift();
    let tags = node.match(/tags:: (?<tags>.*recipe.*)/).groups.tags.split(", ");
    json = {
      ...json,
      [current.hash]: { frontmatter: { ...json.frontmatter, tags } },
    };
    return parse(text, json, current, initialSetup);
  }

  // check for unordered list
  if (/^-/.test(text[0]) || /^\d/.test(text[0])) {
    if (!current.inList) {
      let deepest = findDeepest(current);
      assignNested(text[0], deepest, json, current, "ul");
      current.list = current.count.toString();
      current.inList = true;
      current.count++;
    }
    let deepest = findDeepest(current);
    let node = text.shift();
    assignNested(node, deepest, json, current, "ul");
    current.count++;
    return parse(text, json, current, initialSetup);
  }

  // check for paragraph
  if (/^\w/.exec(text[0])) {
    // if not in paragraph, start one
    if (!current.inParagraph) {
      let deepest = findDeepest(current);
      assignNested(text[0], deepest, json, current, "paragraph");
      current.paragraph = current.count;
      current.inParagraph = true;
      current.count++;
    }
    let deepest = findDeepest(current);
    let node = text.shift();
    assignNested(node, deepest, json, current, "paragraph");
    // find deepest
    return parse(text, json, current, initialSetup);
  }

  // TODO: start of block element `current.inParagraph = false`

  let node = text.shift();
  // handle empty strings or lines
  if (node === "") return parse(text, json, current, initialSetup);

  // check if it's a heading text
  let heading = node.match(/(?<h>#{1,6}) (?<title>.*)/);
  if (heading != null) {
    // # Heading 1
    if (heading.groups.h.length == 1) {
      if (!Object.hasOwn(json, "children")) json[current.hash].children = {};
      current.h1 = current.count.toString();
      json[current.hash].children[current.count] = {
        type: "heading",
        level: 1,
        content: heading.groups.title,
        children: {},
      };
      current.count++;
      return parse(text, json, current, initialSetup);
    }

    // ## Heading2
    if (heading.groups.h.length == 2) {
      if (current.inParagraph) current.inParagraph = false;
      if (current.inList) current.inList = false;
      json[current.hash].children[current.h1].children[current.count] = {
        type: "heading",
        level: 2,
        content: heading.groups.title,
        children: {},
      };
      current.h2 = current.count.toString();
      current.count++;
      return parse(text, json, current, initialSetup);
    }
  }

  // Paragraph
  // if (node) {
  //   let deepest = findDeepest(current);
  //   assignNested(node, deepest, json, current);
  // }

  if (
    json[current.hash]?.children &&
    json[current.hash].children["0"] &&
    json[current.hash].children["0"].children["1"]
  )
    return parse(text, json, current, initialSetup);
}

async function digestMessage(message) {
  const msgUint8 = new TextEncoder().encode(message); // encode as (utf-8) Uint8Array
  const hashBuffer = await crypto.subtle.digest("SHA-256", msgUint8); // hash the message
  const hashArray = Array.from(new Uint8Array(hashBuffer)); // convert buffer to byte array
  const hashHex = hashArray
    .map((b) => b.toString(16).padStart(2, "0"))
    .join(""); // convert bytes to hex string
  return hashHex;
}

function findDeepest(current: Current) {
  if (current.h6) return "h6";
  if (current.h5) return "h5";
  if (current.h4) return "h4";
  if (current.h3) return "h3";
  if (current.h2) return "h2";
  if (current.h1) return "h1";
}

function assignNested(what, where, json, current, type) {
  let { h1, h2, h3, h4, h5, h6, paragraph, inParagraph, list, inList, count } =
    current;
  // if we're inside a paragraph, assign text & content
  let inListOrParagraph = list || paragraph;
  if (inParagraph || inList) {
    let obj = { type: "text", content: what };
    if (where === "h6")
      json[current.hash].children[h1].children[h2].children[h3].children[
        h4
      ].children[h5].children[h6].children[inListOrParagraph].children[count] =
        obj;
    else if (where === "h5")
      json[current.hash].children[h1].children[h2].children[h3].children[
        h4
      ].children[h5].children[inListOrParagraph].children[count] = obj;
    else if (where === "h4")
      json[current.hash].children[h1].children[h2].children[h3].children[
        h4
      ].children[inListOrParagraph].children[count] = obj;
    else if (where === "h3")
      json[current.hash].children[h1].children[h2].children[h3].children[
        inListOrParagraph
      ].children[count] = obj;
    else if (where === "h2")
      json[current.hash].children[h1].children[h2].children[
        inListOrParagraph
      ].children[count] = obj;
    else if (where === "h1")
      json[current.hash].children[h1].children[inListOrParagraph].children[
        count
      ] = obj;
  } else {
    // else create paragraph and store in current
    let obj = { type, children: {} };
    if (where === "h6")
      json[current.hash].children[h1].children[h2].children[h3].children[
        h4
      ].children[h5].children[h6].children[count] = obj;
    else if (where === "h5")
      json[current.hash].children[h1].children[h2].children[h3].children[
        h4
      ].children[h5].children[count] = obj;
    else if (where === "h4")
      json[current.hash].children[h1].children[h2].children[h3].children[
        h4
      ].children[count] = obj;
    else if (where === "h3")
      json[current.hash].children[h1].children[h2].children[h3].children[
        count
      ] = obj;
    else if (where === "h2")
      json[current.hash].children[h1].children[h2].children[count] = obj;
    else if (where === "h1")
      json[current.hash].children[h1].children[count] = obj;
  }
}

/*
"tags:: football, basketball, recipe" -> { type: 'tags', tags: ['football', 'basketball', 'recipe']}
"" -> remove
"# Hot Burrito" -> { type: 'heading', level: 1, text: string, children: {...}}
"## Description" -> { type: 'heading', level: 2, text: string, children: {...}}
"- potato" -> { type: 'list', children: { type: 'listItem', text: string }}
*/

/*
[
"tags:: football, basketball, recipe",
"",
"# Hot Burrito",
"## Description",
"Lorem **ipsum** [dolor](http://somelink.com) sit amet, ...",
"## Ingredients",
"- potato",
"- parmesan",
"- cheddar",
"- cheese",
"- burrito",
"## Instructions",
"1. Put stuff on counter",
"2. Mix stuff",
"3. Bake burrito",
"4. Eat"
]

->

{
  tags: ['football', 'basketball', 'recipe'],
  heading1: {
    text: Heading,
    children: {
      heading2: {
        text: Description
        children: 
      }
    }
  }
}
*/
